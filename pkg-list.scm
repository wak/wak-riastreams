;; Copyright (C) 2010 Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software, you can redistribute it and/or
;; modify it under the terms of the MIT/X11 license.

;; You should have received a copy of the MIT/X11 license along with
;; this program. If not, see
;; <http://www.opensource.org/licenses/mit-license.php>.

(package (wak-riastreams (0) (2007 6 8) (1))
  (depends (srfi-45)
           (wak-common))
  
  (synopsis "lazy streams")
  (description
   "Riastreams is a small library implementing lazy streams"
   "based on the SRFI 45 primitives.")
  
  (libraries
   (sls -> "wak")
   (("riastreams" "private") -> ("wak" "riastreams" "private"))))

;; Local Variables:
;; scheme-indent-styles: (pkg-list)
;; End:
